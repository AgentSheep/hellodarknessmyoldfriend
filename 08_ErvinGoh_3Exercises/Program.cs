﻿using System;

namespace _ErvinGoh_3Exercises
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello:");
            Console.WriteLine("Ervin Goh");

            Console.WriteLine(5 + 5);
            Console.WriteLine(10 / 2);

            Console.WriteLine(1 + 5 + 2);
            Console.WriteLine(2 * 6 + 10);
            Console.WriteLine(6 / 2 * 3);
            Console.WriteLine(10*10-10);
        }

    }
}
